#pragma once

#include <vector>

#include "Common.hpp"
#include "Transformations.hpp"

/*
  A ray is defined parametrically as p(t) = e + td
*/
class Ray {
public:
   glm::vec3 e;
   glm::vec3 d;
   Ray() {}
   Ray(glm::vec3 point, glm::vec3 direction) : e(point), d(direction) {}
   ~Ray() {}
};

struct HitRecord {
   bool hit;
   float t;
   glm::vec3 normal;
   HitRecord() : hit(false), t(0), normal(0) {}
   ~HitRecord() {}
};


class Material {
public: 

   glm::vec3 ambient; 
   glm::vec3 diffuse; 
   glm::vec3 highlight; 
   glm::vec3 reflective;
   float phongExponent;
   Material() : phongExponent(0) {};
   ~Material() {};
};

class Geometry {
public:
   virtual HitRecord hit(Ray ray, float t0, float t1) = 0;
};

class Sphere : public Geometry {
   glm::vec3 c;
   float r;
public:
   Sphere(glm::vec3 center, float radius) : c(center), r(radius) {}
   ~Sphere() {}

   HitRecord hit(Ray ray, float t0, float t1);
};
