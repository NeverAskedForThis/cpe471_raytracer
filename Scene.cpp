#include <string>
#include <iostream>

#include "Scene.hpp"

Intersection hitAnything(const std::vector<Object> &objects, Ray ray, float t0, float t1) {
   bool hit = false;
   Intersection intersect;
//   std::cout << "number of objects: " << objects.size() << std::endl;
   for (int i = 0; i < objects.size(); ++i) {
      const Object* o = &(objects[i]);
      HitRecord hr = o->geometry->hit(ray, t0, t1);
      if (hr.hit) {

         //img.pixel(xpx, ypx, col(glm::vec3(1)));
         if (hit) {
            // std::cout << "overlap at this pixel" << std::endl;
         }
         hit = true;
         intersect.object = o;
         intersect.hitRecord = hr;
         t1 = hr.t;
         
/*         std::cout << "hit object: " << o->id
           << std::endl;*/
      }
   }

   return intersect;
}

glm::vec3 Scene::raycolor(Ray ray, float t0, float t1) {
   return _raycolor(ray, t0, t1, 0);
}

static const int MAX_RECURSIVE_CALLS = 16; 
glm::vec3 Scene::_raycolor(Ray ray, float t0, float t1, int cnt) {
   // i1 is the original point of contact with an object
   // i2 is the possible point of contact with another object shadowing this one
   Intersection in1, in2;
   in1 = hitAnything(objects, ray, t0, t1);
   if (in1.hitRecord.hit) {
      if (cnt > 0) {
         //std::cout << "hit on a recursive call: " << in1.object->id << std::endl;
      }
//      std::cout << "eye ray hit " << in1.object->id << std::endl;
      glm::vec3 p = ray.e + in1.hitRecord.t * ray.d;
      Material *m = (in1.object->material);
      glm::vec3 n = glm::normalize(in1.hitRecord.normal); 
      glm::vec3 color = m->diffuse * m->ambient;
      glm::vec3 d = glm::normalize(ray.d);
      glm::vec3 r = d - 2.0f * glm::dot(d, n) * n;
      

      
      for (int i = 0; i < this->lights.size(); ++i) {
         Light light = this->lights[i];
         glm::vec3 l = (light.position - p);
         float distance = glm::length(l);
         
         in2 = hitAnything(objects, Ray(p, l), 0.001, F_INF);
         if (!in2.hitRecord.hit) {
            // temporary overrides
//          m.highlight = glm::vec3(0);
            glm::vec3 h = glm::normalize(glm::normalize(l) + glm::normalize(-ray.d));

            glm::vec3 diffuse =
               m->diffuse *
               light.color *
               glm::clamp(glm::dot(n, glm::normalize(l)), 0.0f, 1.0f);

            glm::vec3 highlight =
               light.color *
               m->highlight *
               glm::pow(glm::dot(h, n), m->phongExponent);
            color += (diffuse + highlight);
            color = glm::clamp(color, 0.0f, 1.0f);
         }
         else {
/*            std::cout << "light ray to " << in1.object->id
                      << " obstructed by " << in2.object->id
                      << std::endl; */
         }
      } // end for-loop

      if (cnt < MAX_RECURSIVE_CALLS && m->reflective != glm::vec3(0)) {
         // specular reflection recursive call - le grand fromage
         color += m->reflective * _raycolor(Ray(p, r), .01, F_INF, cnt+1);
         // seriously this whole project is just setup for this one call
         // recursion is the best thing ever
      }


      return color;
         
   }
   else {
      return this->background;
   }
}
