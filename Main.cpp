/*
CPE471 Final Project - Raytracer.
By Sajjad Mirza
*/

#include <string>
#include <iostream>
#include <vector>

#include "Common.hpp"
#include "Model.hpp"
#include "Scene.hpp"
#include "Transformations.hpp"
#include "Image.h"

const int width = 800, height = 800;
Image img(width, height);
const glm::vec3 BACKGROUND(0.3, 0.3, 0.3);
const bool DISPLAY_GRID = true;

static int xpx, ypx;

Scene * initScene() {
   Camera cam;
   cam.position = glm::vec3(0, 0, 6);
   cam.lookAt = glm::vec3(0, 0, 0);
   cam.up = glm::vec3(0, 1, 0);
   cam.right = glm::vec3(1.33333, 0, 0);

   Light light, light2, light3;
   light.position = glm::vec3(3, 1, 3);
   light.color = glm::vec3(0.7, 0.7, 0.7);

//   light2.position = glm::vec3(-3, 0, 2);
   light2.position = cam.position;
   light2.color = glm::vec3(0.1, 0.1, 0.1);

   light3.position = glm::vec3(0, 4, 0);
   light3.color = glm::vec3(0.6, 0.0, 0.1);

   Object sphere, sphere2, sphere3, sphere4, sphere5;
   sphere.geometry = new Sphere(glm::vec3(-1.5, 2.5, -1.0), 1);
   sphere.material = new Material();
   sphere.material->diffuse = glm::vec3(0.7, 0.2, 0.5);
   sphere.material->ambient = glm::vec3(0.3, 0.3, 0.3);
   sphere.material->highlight = glm::vec3(0.85, 0.85, 0.85);
   sphere.material->reflective = glm::vec3(0.5, 0.2, 0.1);
   sphere.material->phongExponent = 200;
   sphere.id = "sphere 1";

   sphere2.geometry = new Sphere(glm::vec3(3, 0, 0), 1);
   sphere2.material = new Material();
   sphere2.material->diffuse = glm::vec3(0.5, 0.7, 0.4);
   sphere2.material->ambient = glm::vec3(0.5, 0.5, 0.5); 
   sphere2.material->highlight = glm::vec3(0.55, 0.55, 0.55);
   sphere2.material->reflective = glm::vec3(0.1, 0.3, 0.2);
   sphere2.material->phongExponent = 32;
   sphere2.id = "sphere 2";

   sphere3.geometry = new Sphere(glm::vec3(1, 0, -6), 4);
   sphere3.material = new Material();
   sphere3.material->diffuse = glm::vec3(0.1, 0.3, 0.7);
   sphere3.material->ambient = glm::vec3(0.3, 0.3, 0.3);
   sphere3.material->highlight = glm::vec3(0.75, 0.75, 0.75);
   sphere3.material->reflective = glm::vec3(0.1, 0.2, 0.4);
   sphere3.material->phongExponent = 148;
   sphere3.id = "sphere 3";

   sphere4.geometry = new Sphere(glm::vec3(-2, -1, 2), 0.5);
   sphere4.material = new Material();
   sphere4.material->diffuse = glm::vec3(0.2, 0.2, 0.2);
   sphere4.material->ambient = glm::vec3(0.3, 0.3, 0.3);
   sphere4.material->highlight = glm::vec3(0.95, 0.95, 0.95);
   sphere4.material->reflective = glm::vec3(0.1, 0.1, 0.1);
   sphere4.material->phongExponent = 256;
   sphere4.id = "sphere 4";

   sphere5.geometry = new Sphere(glm::vec3(-3.5, 0, -1), 0.75);
   sphere5.material = new Material();
   sphere5.material->diffuse = glm::vec3(0.5, 0.5, 0.5);
   sphere5.material->ambient = glm::vec3(0.3, 0.3, 0.3);
   sphere5.material->highlight = glm::vec3(0.95, 0.95, 0.95);
   sphere5.material->reflective = glm::vec3(0.1, 0.1, 0.1);
   sphere5.material->phongExponent = 512;
   sphere5.id = "sphere 5";

   Scene *scene = new Scene();
   scene->camera = cam;
   scene->lights.push_back(light);
   scene->lights.push_back(light2);
   scene->lights.push_back(light3);
   scene->objects.push_back(sphere);
   scene->objects.push_back(sphere2);
   scene->objects.push_back(sphere3);
   scene->objects.push_back(sphere4);
   scene->objects.push_back(sphere5);
   
   scene->background = BACKGROUND;

   return scene;
}

color_t col(glm::vec3 v) {
   color_t c;
   c.r = v.x;
   c.g = v.y;
   c.b = v.z;
   c.f = 1.0;
   return c;
}

int main() {

   Scene *scene = initScene();
   glm::vec3 u, v, w; // camera basis vectors
   w = glm::normalize(scene->camera.position - scene->camera.lookAt); // -gaze
   v = glm::normalize(scene->camera.up);
   u = glm::normalize(scene->camera.right);
   
   const float l = -1, r = 1, t = 1, b = -1;

   for (int j = 0; j < height; ++j) { // y
      for (int i = 0; i < width; ++i) { // x
         xpx = i;
         ypx = j;
         float ws, vs, us; // coordinates in the camera basis
         ws = -1; // image plane is always 1 away from the eye in the gaze direction
         us = l + (r - l) * ((i + 0.5) / width);
         vs = b + (t - b) * ((j + 0.5) / height);
         
         glm::vec3 s = scene->camera.position + us * u + vs * v + ws * w;
         // ray goes from camera position through s
         glm::vec3 direction = s - scene->camera.position;
         Ray ray(scene->camera.position, direction);

         glm::vec3 color = scene->raycolor(ray, 0, F_INF);
                                           
         img.pixel(i, j, col(color));

         if ((i % 200 == 0 || j % 200 == 0) && DISPLAY_GRID) {
            img.pixel(i, j, col(glm::vec3(1, 0, 0)));
         }
      }
   }


   img.WriteTga("raycast.tga", false);

   
   return 0;
}
