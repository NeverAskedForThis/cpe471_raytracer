#pragma once

#include "Common.hpp"

class Transform {
};

class Translate : Transform {
public:
   glm::vec3 offset;
};

class Scale : Transform {
public:
   float factor;
};

class Rotate : Transform {
public:
   glm::vec3 axis;
   float angle;
};


