#Raytracer

by Sajjad Mirza

#COMPILATION NOTES
This program requires glm. 
To build and run:
$ make
$ raytracer

The 'raytracer' program outputs to a TGA file. Look for 'raycast.tga'.


## Description
A raytracer written in C++ involving reflective spheres.

## Features
- Parametric spheres that can be arranged in a 3D space
- Ray-sphere intersection
- Phong shading to color the spheres, including multiple lights
- Specular reflection to give the spheres mirror-like qualities

## Images

![A very reflective, mirror-like sphere.](highly_reflective_sphere.png)
![Multiple, moderately reflective spheres.](moderate_reflective_spheres.png)
![All spheres reflective.](all_reflective.png)
![Three lights, one of them red.](3_lights_1_red.jpg)

## Sources
- [Freely available chapter from Fundamentals of Computer Graphics by Shirley et. al.](http://www.cs.utah.edu/~shirley/books/fcg2/rt.pdf)
- [Short instructional document about reflection refraction by Bram de Greve](http://graphics.stanford.edu/courses/cs148-10-summer/docs/2006--degreve--reflection_refraction.pdf)




    
