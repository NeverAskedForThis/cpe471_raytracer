#pragma once

#include <vector>

#include "Common.hpp"
#include "Model.hpp"




/*
  Includes the basis vectors:
  w = normalize(position - lookAt)
  u = normalize(right)
  v = normalize(up)
  The magnitude of right and up define the size of the view window.
*/
class Camera {
public:
   glm::vec3 position;
   glm::vec3 lookAt;
   glm::vec3 up;
   glm::vec3 right;
};

class Light {
public:
   glm::vec3 position;
   glm::vec3 color;
};

class Object {
public:
   Geometry *geometry;
   Material *material;
   std::vector<Transform> transforms;
   std::string id;
};

struct Intersection {
   const Object *object;
   HitRecord hitRecord;
   Intersection() : object(NULL) {}
   Intersection(Object *o, HitRecord hr) : object(o), hitRecord(hr) {}
   ~Intersection() {}
};

Intersection hitAnything(const std::vector<Object> &objects, Ray ray, float t0, float t1);

class Scene {
   glm::vec3 _raycolor(Ray ray, float t0, float t1, int cnt);
public:
   Camera camera;
   std::vector<Light> lights;
   std::vector<Object> objects;
   glm::vec3 background;
   glm::vec3 raycolor(Ray ray, float t0, float t1);

   Scene() {}
   ~Scene() {}
};
