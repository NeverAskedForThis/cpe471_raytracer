CC=gcc
CXX=g++
RM=rm -f
CXXFLAGS=-std=c++98 
CPPFLAGS=-O3
LDFLAGS=-O3
LDLIBS=-lm

SRCS=Main.cpp Model.cpp Image.cpp Scene.cpp
OBJS=Main.o Model.o Image.o Scene.o

all: raytracer

raytracer: $(OBJS)
	$(CXX) $(LDFLAGS) -o raytracer $(OBJS) $(LDLIBS) 

.PHONY: clean
.PHONY: depend

depend: .depend

.depend: $(SRCS)
	rm -f ./.depend
	$(CXX) $(CPPFLAGS) -MM $^>>./.depend;

clean:
	$(RM) $(OBJS)

include .depend
