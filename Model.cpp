#include <algorithm>

#include "Model.hpp"

HitRecord Sphere::hit(Ray ray, float t0, float t1) {
   HitRecord hitrec;
   float discriminant =
      pow(glm::dot(ray.d, ray.e - c), 2)
      - glm::dot(ray.d, ray.d) * (glm::dot(ray.e - c, ray.e - c) - r*r);
   if (discriminant < 0) { // ray does not intersect
      return hitrec;
   }
   
   float a =
      (glm::dot(-ray.d, ray.e - c) + sqrt(discriminant))
      / glm::dot(ray.d, ray.d);
  
   float b = (glm::dot(-ray.d, ray.e - c) - sqrt(discriminant)) // check with minus
      / glm::dot(ray.d, ray.d);

   if (t0 <= a && a <= t1 && t0 <= b && b <= t1) { // both solutions in [t0, t1]
      hitrec.hit = true;
      hitrec.t = std::min(a, b);
   }
   else if (t0 <= a && a <= t1) {
      hitrec.hit = true;
      hitrec.t = a;
   }
   else if (t0 <= b && b <= t1) {
      hitrec.hit = true;
      hitrec.t = b;
   }

   hitrec.normal = 2.0f * ((ray.e + hitrec.t * ray.d) - c);
   return hitrec;
}
